import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { File} from "@ionic-native/file";
import { FileOpener} from "@ionic-native/file-opener";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public file: File, public  fileOpener: FileOpener) {

  }

  public openDocumentConte(){
    let myPath = 'assets/pdf/conte.pdf';
    this.fileOpener.open(this.file.applicationDirectory + myPath, 'application/pdf')
      .then( ()=> console.log('Its work') )
      .catch( (err => {
        alert(JSON.stringify(err));
      }) )
  }

  public  openDocumentSymfony(){
    let myPath = 'assets/pdf/Symfony.pdf';
    this.fileOpener.open(this.file.applicationDirectory + myPath, 'application/pdf')
      .then( ()=> console.log('Its work') )
      .catch( (err => {
        alert(JSON.stringify(err));
      }) )
  }

  public researchOnBook(){

  }

  public bookmarkOnBook(){

  }

  public annotationonBook(){

  }



}
